trigger Sample_Lots_before_vod on Sample_Lot_vod__c (before delete, before insert, before update) {
            
        if (Trigger.isDelete) {
            Map <Id, Sample_Lot_vod__c>  sampLots = 
                new Map<Id, Sample_Lot_vod__c>  ([SELECT Id, 
                    (Select Id from Sample_Transaction_vod__r) 
                        FROM 
                        Sample_Lot_vod__c where Id in : Trigger.old]);
                                                                                                
            for (Integer i = 0; i < Trigger.old.size (); i++) {         
                Sample_Lot_vod__c  thisLot = sampLots.get (Trigger.old[i].Id);          
                Boolean found = false;
                for (Sample_Transaction_vod__c sampTran : thisLot.Sample_Transaction_vod__r) {
                    found = true;
                    break;
                }
                if ( found == true ) 
                    Trigger.old[i].Id.addError (VOD_GET_ERROR_MSG.getErrorMsg('NO_DEL_SAMPLOT_W_TRAN','TriggerError'), false);
            }                                 
        }       
        else if (Trigger.isUpdate) 
        {               
            VOD_ERROR_MSG_BUNDLE bundle = new VOD_ERROR_MSG_BUNDLE();               
            for (Integer i  = 0; i < Trigger.new.size(); i++) {
                if (Trigger.new[i].Name != Trigger.old[i].Name) {
                            Trigger.new[i].Name.addError (bundle.getErrorMsg('NO_UPDATE_FIELD'), false);
                }
                if (Trigger.new[i].OwnerId != Trigger.old[i].OwnerId) {
                    Trigger.new[i].OwnerId.addError (bundle.getErrorMsg('NO_UPDATE_FIELD'), false);
                }
                if (Trigger.new[i].Sample_vod__c != Trigger.old[i].Sample_vod__c) {
                    Trigger.new[i].Sample_vod__c = Trigger.old[i].Sample_vod__c;
                }
                if (Trigger.new[i].Sample_Lot_Id_vod__c != Trigger.old[i].Sample_Lot_Id_vod__c) {
                    Trigger.new[i].Sample_Lot_Id_vod__c.addError (bundle.getErrorMsg('NO_UPDATE_FIELD'), false);
                }       
            } 
        } 
        else // Insert
        {  
            String label  = System.label.USE_MULTI_SAMPLE_vod;             
            Boolean bUseSamp = false;  
            if (label != null && label != 'false') {
                bUseSamp = true;
            } 
            for (Integer i  = 0; i < Trigger.new.size(); i++) {                             
                String ownerid = Trigger.new[i].OwnerId;
                String name = Trigger.new[i].Name; 
                    if (bUseSamp)
                        Trigger.new[i].Sample_Lot_Id_vod__c =  
                            ownerid + '_' + Trigger.new[i].Sample_vod__c.replaceAll(' ', '_') + '_' + name; 
                    else
                        Trigger.new[i].Sample_Lot_Id_vod__c = ownerid + '_' + name; 
            }           
        }                            
    }