public with sharing class BIIB_Case_Handler_Class 
{
    public static void OnBeforeInsert(List<Case> newCaseList) 
    {
        if(newCaseList != null && !newCaseList.isEmpty()) 
        {
            preventCaseDuplicates(newCaseList);
        }
    }
      
    static void preventCaseDuplicates(List<Case> triggerCaseList) 
    {
        //Query custom setting and create a map with reordtype name as key and permission(Not Allowed?) as value 
        Map<String,Boolean> mapCaseTypeToPermission = new Map<String,Boolean>();
        Map<String,Map<String,Integer>> mapAccountIdToCaseRecordTypeToCount = new Map<String,Map<String,Integer>>();
        Map<String,String> mapCaseRecordTypeToDeveloperName = new Map<String,String>();
        for(BIIB_Case_RecordTypes__c caseRecordtypeList : BIIB_Case_RecordTypes__c.getAll().values())
        {
            if(!caseRecordtypeList.BIIB_Multiple_Allowed__c)
                mapCaseTypeToPermission.put(caseRecordtypeList.Name,caseRecordtypeList.BIIB_Multiple_Allowed__c);
        }
        system.debug('--------------'+mapCaseTypeToPermission);
        //loop thorugh the case list and create a set of Account ids
        Set<Id> setAccountId = new Set<Id>();
        for(Case objCase : triggerCaseList)
        {
            if(objCase.AccountId != null) setAccountId.add(objCase.AccountId);
        }
        
        //Querying all Existing case records related to Account and framing a map of Account Id to Map of Case Record Type and Count
        for(Case objCase : [    SELECT RecordType.DeveloperName, RecordType.Id, RecordTypeId, Account.RecordTypeId, AccountId, Account.Name FROM Case 
                                WHERE Account.RecordTypeId =: BIIB_Utility_Class.getRecordTypeId('Account','Patient') AND Status!='Closed'])
        {
            mapCaseRecordTypeToDeveloperName.put(objCase.RecordType.Id,objCase.RecordType.DeveloperName);
            if(!mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId))
            {
                Map<String,Integer> mapCaseRecorTypeToCount = new Map<String,Integer>();
                mapCaseRecorTypeToCount.put(objCase.RecordTypeId,1);
                mapAccountIdToCaseRecordTypeToCount.put(objCase.AccountId,mapCaseRecorTypeToCount);
            }
            else if(mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId) 
            && mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).containsKey(objCase.RecordTypeId))
            {
                Integer count = mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).get(objCase.RecordTypeId);
                count++;
                mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).put(objCase.RecordTypeId,count);
            }
            else if(mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId) 
            && !mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).containsKey(objCase.RecordTypeId))
            {
                mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).put(objCase.RecordTypeId,1);
            } 
        }
        
        system.debug('------mapAccountIdToCaseRecordTypeToCount--------'+mapAccountIdToCaseRecordTypeToCount);
        
        for(Case objCase : triggerCaseList)
        {
            String strRecDeveloperName = mapCaseRecordTypeToDeveloperName.get(objCase.RecordTypeId);
            if(strRecDeveloperName != null && mapCaseTypeToPermission.containsKey(strRecDeveloperName))
            {
                system.debug('------objCase.RecordType.DeveloperName--------'+objCase.RecordType.DeveloperName);
                if(objCase.AccountId != Null && mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId)
                && mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).containsKey(objCase.RecordTypeId) 
                && mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).get(objCase.RecordTypeId) > 0)
                {
                    objCase.addError('There is already open case with the same type on the same patient');
                }
                else if(objCase.AccountId != Null)
                {
                    if(!mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId))
                    {
                        Map<String,Integer> mapCaseRecorTypeToCount = new Map<String,Integer>();
                        mapCaseRecorTypeToCount.put(objCase.RecordTypeId,1);
                        mapAccountIdToCaseRecordTypeToCount.put(objCase.AccountId,mapCaseRecorTypeToCount);
                    }
                    else if(mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId) 
                    && mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).containsKey(objCase.RecordTypeId))
                    {
                        Integer count = mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).get(objCase.RecordTypeId);
                        count++;
                        mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).put(objCase.RecordTypeId,count);
                    }
                    else if(mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId) 
                    && !mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).containsKey(objCase.RecordTypeId))
                    {
                        mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).put(objCase.RecordTypeId,1);
                    }
                }
            }
            else if(objCase.AccountId != Null)
            {
                if(!mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId))
                {
                    Map<String,Integer> mapCaseRecorTypeToCount = new Map<String,Integer>();
                    mapCaseRecorTypeToCount.put(objCase.RecordTypeId,1);
                    mapAccountIdToCaseRecordTypeToCount.put(objCase.AccountId,mapCaseRecorTypeToCount);
                }
                else if(mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId) 
                && mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).containsKey(objCase.RecordTypeId))
                {
                    Integer count = mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).get(objCase.RecordTypeId);
                    count++;
                    mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).put(objCase.RecordTypeId,count);
                }
                else if(mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId) 
                && !mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).containsKey(objCase.RecordTypeId))
                {
                    mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).put(objCase.RecordTypeId,1);
                }
            }
        }           
    }
}