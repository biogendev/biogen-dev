global class BIIB_Escalate_To_Field_Class 
{
	webService static String BIIB_Escalate_To_Field(String strWorkItemId) 
	{
	    
		    system.debug('--------InsideMethodBIIB_Escalate_To_Field------');
		    system.debug('--------strWorkItemId------'+strWorkItemId);
		    String strOutput = '';
		    String strRecordType;
		    List<Work__c> lstWorkItem = new List<Work__c>();
		    List<Work__c> lstWorkRecordType = new List<Work__c>();
		    String strCaseId;
		    Integer intNoOfAttempts;
		    String strWorkItemRecordType;
	
			//Get RecordType of the current Work Item Record
			
			lstWorkRecordType = Database.query('SELECT RecordType.Name FROM Work__c WHERE Id =: strWorkItemId');
			system.debug('--------lstWorkRecordType------'+lstWorkRecordType);
			if(lstWorkRecordType.size() > 0)
			{
				system.debug('--------InsideIfListSize------'+lstWorkRecordType.size());
				for(Work__c objWorkRecordType :lstWorkRecordType)
				{
					system.debug('--------Inside1stFor------');
					strWorkItemRecordType = objWorkRecordType.RecordType.Name;
					system.debug('--------strWorkItemRecordType------'+strWorkItemRecordType);
				}
			}
			for(RecordType rt : [SELECT Id,Name,DeveloperName FROM RecordType WHERE SobjectType = 'Work__c' AND Name =:strWorkItemRecordType])
			{
				system.debug('--------Inside12ndFor------');
				strRecordType = rt.DeveloperName;
				system.debug('--------strRecordType------'+strRecordType);
			}
			
			List<BIIB_Work_Item_RecordType__c> lstWorkItemRecordType = [SELECT BIIB_No_Of_Attempts__c,BIIB_Can_Be_Escalated__c FROM BIIB_Work_Item_RecordType__c WHERE Name =:strRecordType];
		   	system.debug('--------lstWorkItemRecordType------'+lstWorkItemRecordType);
		   	
		   	// Validate if this Work Item Record Type can be escalated or not
		   	for(BIIB_Work_Item_RecordType__c objWorkItemRecordType : lstWorkItemRecordType)
		   	{
		   		system.debug('--------InsideForLoopForWorkItemCustomSetting------');
		   		intNoOfAttempts  = Integer.valueOf(objWorkItemRecordType.BIIB_No_Of_Attempts__c);
		   		system.debug('--------intNoOfAttempts------'+intNoOfAttempts);
		   		
		   		Boolean CanEscalate = objWorkItemRecordType.BIIB_Can_Be_Escalated__c;
		   		system.debug('--------CanEscalate------'+CanEscalate);
		   		
		   		if(objWorkItemRecordType.BIIB_Can_Be_Escalated__c == false)
		   		{
		   			system.debug('--------intNoOfAttempts------'+intNoOfAttempts);
		   			strOutput = 'This Work Item Record Type cannot be escalated';
		   			system.debug('--------strOutput------'+strOutput);
		   			
		   			
		   		}
		   	}
		   	
			// Get Case Id for the Current Work Item Record
			lstWorkItem = Database.query('SELECT Id,BIIB_Case__c from Work__c where Id =:strWorkItemId');
			strCaseId = lstWorkItem[0].BIIB_Case__c;
			
			if(strOutput == '')
			{
				system.debug('--------InsideLoop------');
				for(List<Work__c> lstWork :[SELECT Id FROM Work__c WHERE RecordType.Name =:strWorkItemRecordType AND BIIB_Case__c =:strCaseId AND BIIB_Status__c = 'Closed-Failed'])
				{
					system.debug('----ListSize----'+lstWork.size());
					if(lstWork.size() == intNoOfAttempts)
					{
						strOutput = 'true';
						system.debug('--------strOutput------'+strOutput);
						// Redirect Page to a new page
					}
					
					else
					{
						strOutput = 'The No Of attempts For this Work Item RecordType have not exceeded.Please Create a new Work Item For this record type';
						system.debug('--------strOutput------'+strOutput);
					}
				}
			}
			
			// For the Parent Case for this work Item, query if there are Work Item Records of the same Record Type and Status is Closed. If the count of such records is 
			//same as the "No Of Attempts" in custom setting, then allow to create escalate to field WI, else return an error
			


	   return strOutput;
  	}
}