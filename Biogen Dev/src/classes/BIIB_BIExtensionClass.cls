public with sharing class BIIB_BIExtensionClass {

    public List<Patient_Plan__c>BIList{get;set;}
    public List<Patient_Plan__c>localBIList{get;set;}
    public String strBIId {get;set;}
    public boolean biTableRendered {get;set;}
    public String patientId = '';
    public String patientName = '';
    public Id recTypeId=null;
    
    private Integer pageNumber;
    private Integer pageSize;
    private Integer totalPageNumber;
    
    
    public BIIB_BIExtensionClass(ApexPages.StandardController controller)
     {
        recTypeId = BIIB_Utility_Class.getRecordTypeId('Patient_Plan__c','Benefit Investigation');
        Case Owork =(Case)controller.getRecord();
        biTableRendered = false;
        List<Case> cList = new List<Case>();
        cList = [select Id, AccountId,Account.Name from Case where Id=:Owork.Id limit 1];
        if(!(cList.isEmpty())){
            patientId = cList[0].AccountId;
            patientName=cList[0].Account.Name;
            patientName=patientName.replace(' ', '+');
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Service Request is not linked to valid patient ');
            ApexPages.addMessage(myMsg);
            
        }
        pageNumber = 0;
        totalPageNumber = 0;
        pageSize = Integer.valueOf(Label.BIIB_Page_Size_Infusion_Search);
        localBIList = [Select Id, Name, BIIB_Patient_Lookup__c, BIIB_Plan__c, BIIB_Effective_Date__c, BIIB_End_Date__c from Patient_Plan__c where BIIB_Patient_Lookup__c = :patientId and recordtype.Id=:recTypeId];

        if(localBILIst.isEmpty()){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Patient is not linked to any Plan. Please click on the new button to associate a plan ');
            ApexPages.addMessage(myMsg);            
        }
        system.debug('*'+ApexPages.currentPage().getParameters().get('Id'));
        BindData(1);
     }
    
    
    
    
    public void BindData(Integer newPageIndex)
    {
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;

            }
            
            pageNumber = newPageIndex;
            BIList = new List<Patient_Plan__c>();
            for(Integer i=min; i < max ; i++)
            {
                if (localBIList.size() > i)
                    BIList.add(localBIList[i]);
            }
            
            if(!BIList.isEmpty() )
            {
                biTableRendered=true;
            }else{
                biTableRendered=false;
            }
    }
    public PageReference editBI(){
        PageReference p = new PageReference('/'+strBIId+'/e?retURL=%2Fa'+strBIId);
        return p;
    } 
    public PageReference newBI(){
        
        if(recTypeId != null) {
            String keyprefixPP = Schema.getGlobalDescribe().get('Patient_Plan__c').getDescribe().getKeyPrefix();
            BIIB_Object_FieldId__c patientField= BIIB_Object_FieldId__c.getInstance('Patient_Plan__c_Patient__c');
            PageReference p = new PageReference('/'+keyprefixPP +'/e?retURL=%2F'+keyprefixPP+'%2Fo&RecordType='+recTypeId+'&'+patientField.BIIB_Field_Id__c+'='+patientName+'&'+ patientField.BIIB_Field_Id__c+'_lkid='+patientId);
            return p;
         }else{
            return null;
         }  
    }   
    //pagination
    public Integer getPageNumber()
    {
        return pageNumber;
    }
    
    public Integer getPageSize()
    {
        return pageSize;
    }
    
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }
    
    public Boolean getNextButtonDisabled()
    {
        if (localBIList == null) return true;
        else
        return ((pageNumber * pageSize) >= localBIList.size());
    }
    
    public Integer getTotalPageNumber()
    {

        totalPageNumber = localBIList.size() / pageSize;
        Integer mod = localBIList.size() - (totalPageNumber * pageSize);
        if (mod > 0)
            totalPageNumber++;
        return totalPageNumber;
    }
    
    public PageReference nextBtnClick()
     {
        BindData(pageNumber + 1);
        return null;
    }
    
    public PageReference previousBtnClick()
     {
        BindData(pageNumber - 1);
        return null;
    }
    
}