public with sharing class BIIB_Affiliation_Handler
{
    public Static Boolean isReverseAffiliationInserted = false;
    public static void onBeforeInsert(List<BIIB_Affiliation__c> newList)
    {
        assignRecordType(newList);
        if(!isReverseAffiliationInserted)
            createReverseAffiliation(newList);
    }
    
    public static void createReverseAffiliation(List<BIIB_Affiliation__c> lstAffiliation)
    {
        List<BIIB_Affiliation__c> lstRevAffiliation = new List<BIIB_Affiliation__c>();
        for(BIIB_Affiliation__c objAffiliation : lstAffiliation)
        {
            BIIB_Affiliation__c objAff = new BIIB_Affiliation__c();
            objAff = objAffiliation.clone();
            objAff.BIIB_To_Account__c = objAffiliation.BIIB_From_Account__c;
            objAff.BIIB_From_Account__c = objAffiliation.BIIB_To_Account__c;
            lstRevAffiliation.add(objAff);
        }
        
        if(lstRevAffiliation.size() > 0)
        {
            isReverseAffiliationInserted = true;
            insert lstRevAffiliation;
        }    
    }
    
    public static void onBeforeUpdate(Map<Id,BIIB_Affiliation__c> mapNewAffiliations)
    {
        assignRecordType(mapNewAffiliations.values());
    }
    
    public static void assignRecordType(List<BIIB_Affiliation__c> lstAffiliation)
    {
        String strPatientHCPId = BIIB_Utility_Class.getRecordTypeId('BIIB_Affiliation__c', 'Patient - HCP');
        String strPatientSPPId = BIIB_Utility_Class.getRecordTypeId('BIIB_Affiliation__c', 'Patient - SPP');
        String strPatientInfusionSiteId = BIIB_Utility_Class.getRecordTypeId('BIIB_Affiliation__c', 'Payer - Infusion Site');
        String strPayerInfusionSiteId = BIIB_Utility_Class.getRecordTypeId('BIIB_Affiliation__c', 'Payer - Infusion Site');
        
        for(BIIB_Affiliation__c objAffiliation : lstAffiliation)
        {
            if((objAffiliation.BIIB_To_Account_Type__c == 'Patient' || objAffiliation.BIIB_From_Account_Type__c == 'Patient') 
            && (objAffiliation.BIIB_To_Account_Type__c == 'HCP' || objAffiliation.BIIB_From_Account_Type__c == 'HCP'))
            {
                //patient-hcp
                objAffiliation.RecordTypeId = strPatientHCPId;
            }
            else if((objAffiliation.BIIB_To_Account_Type__c == 'Patient' || objAffiliation.BIIB_From_Account_Type__c == 'Patient') 
            && (objAffiliation.BIIB_To_Account_Type__c == 'Infusion Sites' || objAffiliation.BIIB_From_Account_Type__c == 'Infusion Sites'))
            {
                //patient-infusion site
                objAffiliation.RecordTypeId = strPatientInfusionSiteId;
            }
            else if((objAffiliation.BIIB_To_Account_Type__c == 'Patient' || objAffiliation.BIIB_From_Account_Type__c == 'Patient') 
            && (objAffiliation.BIIB_To_Account_Type__c == 'SPP' || objAffiliation.BIIB_From_Account_Type__c == 'SPP'))
            {
                //patient-SPP
                objAffiliation.RecordTypeId = strPatientSPPId;
            }
            else if((objAffiliation.BIIB_To_Account_Type__c == 'Payer' || objAffiliation.BIIB_From_Account_Type__c == 'Payer') 
            && (objAffiliation.BIIB_To_Account_Type__c == 'Infusion Sites' || objAffiliation.BIIB_From_Account_Type__c == 'Infusion Sites'))
            {
                //Payer-Infusion Site
                objAffiliation.RecordTypeId = strPayerInfusionSiteId;
            }
            else
            {
                objAffiliation.addError('You can create Affiliation between Patient - HCP, Patient - Infusion Sites, Patient - SPP and Payer - Infusion Site');
            }
        }
    }
    
    public static void onAfterInsert(Map<Id,BIIB_Affiliation__c> mapNewAffiliations)
    {
        Set<Id> setFromAccId = new Set<Id>();
        Set<Id> setToAccount = new Set<Id>();
        Set<Id> setAffiliationId = new Set<Id>();
        Map<String,BIIB_Affiliation__c> mapAccToFromToAffiliationObj = new Map<String,BIIB_Affiliation__c>();
        Set<Id> setValidateFromAccountId = new Set<Id>();
        
        for(BIIB_Affiliation__c objAff : mapNewAffiliations.values())
        {
            if(objAff.BIIB_Primary__c)
            {
                setFromAccId.add(objAff.BIIB_From_Account__c);
                setAffiliationId.add(objAff.Id);
                
                setToAccount.add(objAff.BIIB_To_Account__c);
                mapAccToFromToAffiliationObj.put(objAff.BIIB_To_Account__c+'-'+objAff.BIIB_From_Account__c, objAff);
                setValidateFromAccountId.add(objAff.BIIB_From_Account__c);
            }
        }
        
        if(setToAccount.size() > 0 && mapAccToFromToAffiliationObj.size() > 0)
            validatePrimaryAffiliation(mapAccToFromToAffiliationObj, setToAccount, setValidateFromAccountId);
        
        if(setAffiliationId.size() > 0 && setFromAccId.size() > 0)
            evaluatePrimaryCheckbox(setAffiliationId,setFromAccId);
    }
    
    public static void onAfterUpdate(Map<Id,BIIB_Affiliation__c> mapNewAffiliations, Map<Id,BIIB_Affiliation__c> mapOldAffiliations)
    {
        Set<Id> setFromAccId = new Set<Id>();
        Set<Id> setAffiliationId = new Set<Id>();
        
        Set<Id> setToAccount = new Set<Id>();
        Map<String,BIIB_Affiliation__c> mapAccToFromToAffiliationObj = new Map<String,BIIB_Affiliation__c>();
        Set<Id> setValidateFromAccountId = new Set<Id>();
        
        String strPatientHCPId = BIIB_Utility_Class.getRecordTypeId('BIIB_Affiliation__c', 'Patient - HCP');
        for(BIIB_Affiliation__c objAff : mapNewAffiliations.values())
        {
            
            if(objAff.BIIB_Primary__c != mapOldAffiliations.get(objAff.Id).BIIB_Primary__c && objAff.BIIB_Primary__c 
            && objAff.RecordTypeId == strPatientHCPId) 
            {
                setFromAccId.add(objAff.BIIB_From_Account__c);
                setAffiliationId.add(objAff.Id);
            }
            //Validation related to primary checkbox needs to be done for Patient - HCP record type only.
            else if(objAff.BIIB_Primary__c != mapOldAffiliations.get(objAff.Id).BIIB_Primary__c && !objAff.BIIB_Primary__c
            && objAff.RecordTypeId == strPatientHCPId) 
            {
                setToAccount.add(objAff.BIIB_To_Account__c);
                mapAccToFromToAffiliationObj.put(objAff.BIIB_To_Account__c+'-'+objAff.BIIB_From_Account__c, objAff);
                setValidateFromAccountId.add(objAff.BIIB_From_Account__c);
            }
        }
        if(setToAccount.size() > 0 && mapAccToFromToAffiliationObj.size() > 0)
            validatePrimaryAffiliation(mapAccToFromToAffiliationObj, setToAccount, setValidateFromAccountId);
        
        if(setAffiliationId.size() > 0 && setFromAccId.size() > 0)
            evaluatePrimaryCheckbox(setAffiliationId,setFromAccId);
    }
    
    public static void validatePrimaryAffiliation(Map<String,BIIB_Affiliation__c> mapAccToFromToAffiliationObj, Set<Id> setToAccountId, Set<Id> setFromAccountId)
    {
        for(Work__c objWork : [ SELECT Id, BIIB_Account__c, BIIB_Case__r.AccountId  FROM Work__c WHERE BIIB_Status__c != 'Completed' AND BIIB_Case__r.AccountId IN : setFromAccountId AND BIIB_Case__r.Is_Patient_DND__c = true])
        {
            if(mapAccToFromToAffiliationObj.containsKey(objWork.BIIB_Account__c+'-'+objWork.BIIB_Case__r.AccountId))
                mapAccToFromToAffiliationObj.get(objWork.BIIB_Account__c+'-'+objWork.BIIB_Case__r.AccountId).addError('You cannot mark Affiliation as non primary as there are open work pending with its related HCP');
        }
    }
    
    public static void evaluatePrimaryCheckbox(Set<Id> setAffiliationId, Set<Id> setFromAccId)
    {
        List<BIIB_Affiliation__c> lstAffiliation = new List<BIIB_Affiliation__c>();
        for(BIIB_Affiliation__c  objAff : [ SELECT Id, BIIB_Primary__c FROM BIIB_Affiliation__c 
                                            WHERE BIIB_Primary__c = true AND Id Not IN : setAffiliationId AND BIIB_From_Account__c IN :setFromAccId])
        {
            objAff.BIIB_Primary__c = false;
            lstAffiliation.add(objAff);
        }
        
        if(lstAffiliation.size() > 0)
        {
            update lstAffiliation;
        }
    }
}